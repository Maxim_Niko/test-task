<?php
/**
 * Created by PhpStorm.
 * User: Maxim
 * Date: 23.04.2018
 * Time: 22:32
 */

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var \app\models\Menu[] $menus
 */

?>

<h1>Menus</h1>
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
    Create menu
</button>
<?= \app\widgets\Menu\MenuWidget::widget(['menus' => $menus]) ?>
<!-- Button trigger modal -->

<div class="modal fade"  id="myModal"  tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <?= $this->render('_form', ['model' => $form]) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->