<?php
/**
 * Created by PhpStorm.
 * User: Maxim
 * Date: 23.04.2018
 * Time: 22:32
 */

/**
 * @var \yii\web\View $this
*/
?>

<?php $form = \yii\bootstrap\ActiveForm::begin() ?>

<?= $form->field($model, 'parent_id')->dropDownList(\app\models\Menu::getListAll(), ['prompt' => 'Select menu...']) ?>
<?= $form->field($model, 'name')->textInput(['placeholder' => true, 'max-length' => true])->label(false) ?>
<?= $form->field($model, 'uri')->textInput(['placeholder' => true, 'max-length' => true])->label(false) ?>
<?= \yii\helpers\Html::submitInput('Create') ?>
<?php \yii\bootstrap\ActiveForm::end() ?>
