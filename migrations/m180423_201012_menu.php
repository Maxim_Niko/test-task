<?php

use yii\db\Migration;

/**
 * Class m180423_201012_menu
 */
class m180423_201012_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%menu}}', [
            'id' => $this->primaryKey()->unsigned(),
            'parent_id' => $this->integer()->unsigned(),
            'name' => $this->string('100'),
            'uri' => $this->string('100'),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%menu}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180423_201012_menu cannot be reverted.\n";

        return false;
    }
    */
}
