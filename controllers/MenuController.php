<?php

namespace app\controllers;

use app\Forms\MenuForm;
use app\models\Menu;
use app\Services\MenuService;
use Yii;
use yii\base\Module;
use yii\web\Controller;
use yii\filters\VerbFilter;

class MenuController extends Controller
{
    private $service;

    public function __construct(string $id, Module $module, array $config = [], MenuService $menuService)
    {
        parent::__construct($id, $module, $config);
        $this->service = $menuService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $form = new MenuForm();

        if (Yii::$app->getRequest()->isPost && $form->load(Yii::$app->getRequest()->post())) {
            if ($form->validate()) {
                try {
                    $this->service->create($form);
                    Yii::$app->session->setFlash('success', 'Меню добавлено успешно!');
                    return $this->redirect(Yii::$app->getRequest()->referrer);
                } catch (\DomainException $e) {
                    Yii::$app->errorHandler->logException($e);
                }
            } else {
                Yii::$app->session->setFlash('error', implode('<br>', $form->getErrors()));
            }
        }

        return $this->render('index', ['menus' => Menu::getParents(), 'form' => $form]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        if (Yii::$app->getRequest()->isPost) {
            if ($model = Menu::findOne($id)) {
                try {
                    $count = $this->service->remote($model);
                    Yii::$app->session->setFlash('success', "Удалено успешно ({$count} шт.)");
                } catch (\DomainException $e) {
                    Yii::$app->errorHandler->logException($e);
                }
            } else {
                Yii::$app->session->setFlash('error', 'Пункт меню не найден');
            }
        }

        return $this->redirect(Yii::$app->getRequest()->referrer);
    }

}
