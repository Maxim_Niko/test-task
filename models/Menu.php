<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $uri
 *
 * @property Menu $parent
 * @property Menu[] $childs
 *
 * Class Menu
 * @package app\models
 */
class Menu extends ActiveRecord
{

    public static function tableName(): string
    {
        return '{{%menu}}';
    }

    public static function create($name, $uri, $parent_id): self
    {
        $menu = new self();

        $menu->name = $name;
        $menu->uri = $uri;
        $menu->parent_id = $parent_id;

        return $menu;
    }

    public function getChilds(): ActiveQuery
    {
        return $this->hasMany(self::class, ['parent_id' => 'id']);
    }

    public function getParent(): ActiveQuery
    {
        return $this->hasOne(self::class, ['parent_id' => 'id']);
    }

    public static function getListAll(): array
    {
        return self::find()
            ->select(['CONCAT(name, \' (\', uri ,\')\')', 'id'])
            ->indexBy('id')
            ->column();
    }

    public static function getParents(): array
    {
        return self::find()->alias('m')
            ->joinWith(['childs ch'])
            ->andWhere(
                ['m.id' => self::find()->select(['DISTINCT(parent_id)'])->andWhere(['is not', 'parent_id', null])->column()]
            )
            ->andWhere(
                ['not in', 'm.id', self::find()->select(['id'])->andWhere(['is not', 'parent_id', null])->column()]
            )
            ->union(self::find()->andWhere(['is', 'parent_id', null]))
            ->all();
    }

}
