<?php

namespace app\widgets\Menu;

use app\models\Menu;
use app\widgets\Menu\Assets\MenuAsset;
use http\Url;
use yii\helpers\Html;

class MenuWidget extends \yii\bootstrap\Widget
{
    /**
     * @var Menu[] $menus
     */
    public $menus;

    public function init()
    {
        parent::init();
        if (!is_array($this->menus)) {
            $this->menus = [];
        }
    }

    public function run()
    {
        MenuAsset::register($this->view);

        return $this->renderItems($this->menus);
    }


    private function renderItems(array $menus, $level = 0): string
    {
        $content = '';

        foreach ($menus as $menu) {
            if ($level < 7) {
                $col = 12 - $level;
                $offset = $level;
            }
            $content .= $this->renderItem($menu, $col, $offset, $level);
        }

        return Html::tag('div', $content, ['class' => 'row']);
    }

    private function renderItem(Menu $menu, int $col, int $offset, int $level): string
    {
        $childs = '';
        if ($menu->childs) {
            $childs .= $this->renderItems($menu->childs, ++$level);
        }
        return Html::tag('div',
            "{$menu->name} ({$menu->uri})" .
            Html::tag('span', null, ['class' => 'glyphicon glyphicon-plus add', 'data-id' => $menu->id, 'onClick' => 'addMenu(this)']) .
            Html::a(null, \yii\helpers\Url::to(['menu/delete', 'id' => $menu->id]),
                [
                'class' => 'glyphicon glyphicon-trash',
                'data-method' => 'post',
                'data-confirm' => 'Are you sure you want to delete this item?'
            ]) .
            $childs,
            ['class' => "col-md-{$col} col-md-offset-{$offset}"]);
    }
}
