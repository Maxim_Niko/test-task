<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\widgets\Menu\Assets;

use app\assets\AppAsset;
use yii\web\AssetBundle;

class MenuAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/Menu/js';
    public $baseUrl = '@web';

    public $js = [
        'add-menu.js'
    ];

    public $depends = [
        AppAsset::class
    ];
}
