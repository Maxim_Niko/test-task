<?php
/**
 * Created by PhpStorm.
 * User: Maxim
 * Date: 24.04.2018
 * Time: 7:43
 */

namespace app\Services;

use app\Forms\MenuForm;
use app\models\Menu;

class MenuService
{
    private $transaction;

    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @param MenuForm $menuForm
     * @return Menu
     */
    public function create(MenuForm $menuForm): Menu
    {
        $menu = Menu::create(
            $menuForm->name,
            $menuForm->uri,
            !empty($menuForm->parent_id) ? $menuForm->parent_id : null
        );

        if (!$menu->save()) {
            throw new \RuntimeException('Saving error.');
        }

        return $menu;
    }

    /**
     * @param Menu $menu
     * @return int
     * @throws \Exception
     */
    public function remote(Menu $menu): int
    {
        $count = 1;
        $this->transaction->wrap(function () use (&$count, $menu) {
            if ($childs = $menu->childs) {
                $count += $this->remoteChilds($childs);
            }

            if (!$menu->delete()) {
                throw new \RuntimeException('Deleting error.');
            }
        });

        return $count;

    }

    /**
     * @param Menu[] $childs
     * @return int
     */
    private function remoteChilds(array $childs): int
    {
        $ids = [];

        $count = 0;
        foreach ($childs as $menu) {
            if ($menu->childs) {
                $count += $this->remoteChilds($menu->childs);
            }
            $ids[] = $menu->id;
        }
        return $count + Menu::deleteAll(['id' => $ids]);
    }
}