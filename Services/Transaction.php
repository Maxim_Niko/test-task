<?php
/**
 * Created by PhpStorm.
 * User: Maxim
 * Date: 24.04.2018
 * Time: 22:24
 */

namespace app\Services;


class Transaction
{
    public function wrap(callable $func)
    {
        $transaction = \Yii::$app->db->beginTransaction();

        try {
            $func();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}