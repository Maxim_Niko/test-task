<?php
/**
 * Created by PhpStorm.
 * User: Maxim
 * Date: 24.04.2018
 * Time: 7:45
 */

namespace app\Forms;

use app\models\Menu;
use yii\base\Model;

class MenuForm extends Model
{

    public $name;
    public $uri;
    public $parent_id;


    public function rules(): array
    {
        return [
            [['name', 'uri'], 'required'],
            [['name', 'uri'], 'string', 'max' => 100],
            [['uri'], 'url'],
            [['parent_id'], 'exist', 'targetClass' => Menu::class, 'targetAttribute' => ['parent_id' => 'id'], 'skipOnEmpty' => true],
        ];
    }

}